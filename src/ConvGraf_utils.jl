
"""

ConvGraf_utils.jl

Esse arquivo cria funcoes genericas e de tratamento dos arquivos do tipo Graf da PSR
para o modulo principal.

"""


#---------------------------------#
#----    Funcoes Genericas    ----#
#---------------------------------#

#--- set_eol: Funcao para definir caracter de fim de linha
function set_eol( self::Any = nothing )
    if Sys.iswindows()
        return( "\r\n" )
    elseif Sys.islinux()
        return( "\n" )
    elseif Sys.isapple()
        return( "\r" )
    else
        println( "\n\n NOT READY FOR THE CURRENT OS")
        exit()
    end
end

#--- write_log: Funcao para escrever no log
function write_log( path::String , msg::Array{String,1} , language::Int , log_name::String ; flag_console::Int = 1 , flag_file::Int = 1  , write_type::String = "a" )
    
    #--- Caracter de fim de linha
    EOL = set_eol()

    #--- Exibe mensagem no console
    if flag_console == 1
        Base.print( string( msg[ language ] , EOL ) )
    end

    #--- Grava mensagem no arquivo de log
    if flag_file == 1
        LogFile = open( joinpath( path , log_name ) , write_type )
        Base.write( LogFile , string( msg[ language ] , EOL ) )
        Base.close( LogFile )
    end

    return nothing
end

#--- check_path: Funcao para verificar o path informado
function check_path( path::String )

    #--- Normalizacao do path
    norm_path = Sys.iswindows() == true ? normpath( string( path , "\\" ) ) : normpath( string( path , "/" ) )

    #--- Verificando existencia do path informado
    if !isdir( norm_path )
        return -1
    else
        return 0
    end
end

#--- constr_instruc_var: Funcao construtura de variaveis do tipo InstrConvGraf
function constr_instruc_var( path::String )
    TempInstruc = InstrConvGraf()
    TempInstruc.PathOutput = path
    return TempInstruc
end

#--- get_block_stage: Funcao para obter numero de blocos em cada etapa
function get_block_stage( ini_stg::Int , ini_year::Int , num_stg::Int , num_blc::Int , simh::Int , stg_type::Int )

    #================================
    #---  Definicao de variaveis  ---
    ================================#

    local TempBlcStg    ::  Vector{Int}
    local iStg          ::  Int
    local iDate         ::  Date
    
    TempBlcStg = zeros( Int , num_stg )

    #==================
    #---  Rotinas  ---
    ==================#

    if simh == 0                # Caso com simulacao por bloco de demanda
        for iStg in 1:num_stg
            TempBlcStg[ iStg ] = num_blc
        end
    else                        # Caso com simulacao horaria
        if stg_type == 1        # Caso com etapas semanais
            for iStg in 1:num_stg
                TempBlcStg[ iStg ] = 168
            end
        elseif stg_type == 2    # Caso com etapas mensais
            for iStg in 1:num_stg
                iDate               = Date( ini_year , ini_stg , 1 ) + Dates.Month( iStg - 1 )
                TempBlcStg[ iStg ]  = Dates.daysinmonth( iDate ) * 24
            end
        else                    # Caso com etapas trimestrais
            iDate = Date( ini_year , ( ini_stg - 1 ) * 3 + 1 )
            for iStg in 1:num_stg
                iCont = 0
                for iMonth in 1:3
                    iCont += Dates.daysinmonth( iDate ) * 24
                    iDate = iDate + Dates.Month( 1 )
                end
                TempBlcStg[ iStg ] = iCont
            end
        end
    end

    return TempBlcStg
end

#--- constr_output_file: Funcao para configurar arquivo de saida
function constr_output_file( instr::InstrConvGraf )

    iograf              = constr_graph( instr.PathOutput , instr.FileOutput )
    iograf.Ext          = 1
    iograf.IniStg       = instr.IniStg
    iograf.IniYear      = instr.IniYear
    iograf.NumStg       = instr.NumStg
    iograf.NumSer       = instr.NumSer
    iograf.NumBlc       = instr.NumBlc
    iograf.NumAgentes   = length( instr.Agentes )
    iograf.Unit2        = instr.Unidade
    iograf.StgType      = instr.TipoStg
    iograf.Simb         = instr.Simb
    iograf.Sims         = instr.Sims
    iograf.Simh         = instr.Simh
    iograf.Agentes      = instr.Agentes

    return iograf
end

