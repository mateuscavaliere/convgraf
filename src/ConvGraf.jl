module ConvGraf

#--- Carregando bibliotecas
using Libdl
using Dates
using PSRClassesJulia

export InstrConvGraf , read_instruc_file , generate_graf_file , convgraf_main

#--- Incluindo outros sources
include( joinpath( dirname( pathof( PSRClassesJulia ) ) , "PSRClasses.jl" ) )
include( joinpath( dirname(@__FILE__) , "ConvGraf_types.jl" ) )
include( joinpath( dirname(@__FILE__) , "ConvGraf_utils.jl" ) )
include( joinpath( dirname(@__FILE__) , "ConvGraf_fun.jl" ) )

end # module
