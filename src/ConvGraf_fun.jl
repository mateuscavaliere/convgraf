
"""
ConvGraf_fun.jl

Esse arquivo cria funcoes de leitura e tratamento dos arquivos.

"""

#=======================
---  Funcoes de I/O  ---
=======================#

#--- read_instruc_file: Funcao para leitura do arquivo convgrafXXX.dat
function read_instruc_file!( path::String , file_name::String , instr::InstrConvGraf )
    
    #================================
    #---  Definicao de variaveis  ---
    ================================#
    
    local TempIntVal :: Int
    local iofile    ::  IOStream
    local iodata    ::  Array{String,1}
    
    instr.FilesInput    = String[]
    instr.Agentes       = String[]

    #==================
    #---  Rotinas  ---
    ==================#

    #--- Verifica existencia do arquivo
    if !isfile( joinpath( path , file_name ) ) 
        println( string( "  ERROR: File not found - " , joinpath( path , file_name ) ) )
        return -1
    end

    #--- Leitura dos dados do arquivo
    try
        iofile = open( joinpath( path , file_name ) , "r" )
        iodata = readlines( iofile )
        Base.close( iofile )
    catch
        println( string( "  ERROR: Access denied - " , joinpath( path , file_name ) ) )
        return -1
    end

    #--- Verifica consistencia do arquivo
    if length( iodata ) < 14
        println( string( "  ERROR: Incorrect file format - " , joinpath( path , file_name ) ) )
        return -1
    end

    #--- Percorre dados

    #- Diretorio com dados de entrada
    try
        instr.PathInput = rstrip( iodata[ 1 ][ 27:end ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 1 - " , joinpath( path , file_name ) ) )
        return -1
    end
    
    #- Arquivos de entrada
    try
        TempStrFiles = split( iodata[ 2 ][ 27:end ] , ',' )
        for iStr in TempStrFiles
            push!( instr.FilesInput , iStr )
        end
    catch
        println( string( "  ERROR: Incorrect file format. Line 2 - " , joinpath( path , file_name ) ) )
        return -1        
    end

    #- Arquivo de saida
    try
        instr.FileOutput = rstrip( iodata[ 3 ][ 27:end ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 3 - " , joinpath( path , file_name ) ) )
        return -1
    end
    
    #- Tipo de etapa
    try
        TempIntVal = parse( Int , iodata[ 11 ][ 27:27 ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 11 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if ( ( TempIntVal < 1 ) | ( TempIntVal > 3 ) )
        println( string( "  ERROR: Invalid flag. Line 11 - " , joinpath( path , file_name ) ) )
        return -1
    else
        instr.TipoStg = TempIntVal
    end

    #- Etapa inicial
    try
        TempIntVal = parse( Int , iodata[ 4 ][ 27:end ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 4 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if instr.TipoStg == 1
        if ( ( TempIntVal < 1 ) | ( TempIntVal > 52 ) )
            println( string( "  ERROR: Invalid initial stage. Line 4 - " , joinpath( path , file_name ) ) )
            return -1
        else
            instr.IniStg = TempIntVal
        end
    elseif instr.TipoStg == 2
        if ( ( TempIntVal < 1 ) | ( TempIntVal > 12 ) )
            println( string( "  ERROR: Invalid initial stage. Line 4 - " , joinpath( path , file_name ) ) )
            return -1
        else
            instr.IniStg = TempIntVal
        end
    elseif instr.TipoStg == 3
        if ( ( TempIntVal < 1 ) | ( TempIntVal > 4 ) )
            println( string( "  ERROR: Invalid initial stage. Line 4 - " , joinpath( path , file_name ) ) )
            return -1
        else
            instr.IniStg = TempIntVal
        end
    end

    #- Ano inicial
    try
        TempIntVal = parse( Int , iodata[ 5 ][ 27:end ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 5 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if TempIntVal < 1
        println( string( "  ERROR: Invalid initial year. Line 5 - " , joinpath( path , file_name ) ) )
        return -1
    else
        instr.IniYear = TempIntVal
    end

    #- Numero de etapas
    try
        TempIntVal = parse( Int , iodata[ 6 ][ 27:end ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 6 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if TempIntVal < 1
        println( string( "  ERROR: Invalid number of stages. Line 6 - " , joinpath( path , file_name ) ) )
        return -1
    else
        instr.NumStg = TempIntVal
    end

    #- Numero de cenarios
    try
        TempIntVal = parse( Int , iodata[ 7 ][ 27:end ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 7 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if TempIntVal < 1
        println( string( "  ERROR: Invalid number of scenarios. Line 7 - " , joinpath( path , file_name ) ) )
        return -1
    else
        instr.NumSer = TempIntVal
    end

    #- Numero de blocos
    try
        TempIntVal = parse( Int , iodata[ 8 ][ 27:end ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 8 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if TempIntVal < 1
        println( string( "  ERROR: Invalid number of blocks. Line 8 - " , joinpath( path , file_name ) ) )
        return -1
    else
        instr.NumBlc = TempIntVal
    end

    #- Unidade
    try
        instr.Unidade = rstrip( iodata[ 9 ][ 27:end ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 9 - " , joinpath( path , file_name ) ) )
        return -1
    end

    #- Agentes
    try
        TempStrFiles = split( iodata[ 10 ][ 27:end ] , ',' )
        for iStr in TempStrFiles
            push!( instr.Agentes , iStr )
        end
    catch
        println( string( "  ERROR: Incorrect file format. Line 10 - " , joinpath( path , file_name ) ) )
        return -1        
    end
    
    #- Varia por bloco
    try
        TempIntVal = parse( Int , iodata[ 12 ][ 27:27 ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 12 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if ( ( TempIntVal != 0 ) & ( TempIntVal != 1 ) )
        println( string( "  ERROR: Invalid flag. Line 12 - " , joinpath( path , file_name ) ) )
        return -1
    else
        instr.Simb = TempIntVal
    end

    #- Varia por cenario
    try
        TempIntVal = parse( Int , iodata[ 13 ][ 27:27 ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 13 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if ( ( TempIntVal != 0 ) & ( TempIntVal != 1 ) )
        println( string( "  ERROR: Invalid flag. Line 13 - " , joinpath( path , file_name ) ) )
        return -1
    else
        instr.Sims = TempIntVal
    end

    #- Simulacao horaria
    try
        TempIntVal = parse( Int , iodata[ 14 ][ 27:27 ] )
    catch
        println( string( "  ERROR: Incorrect file format. Line 14 - " , joinpath( path , file_name ) ) )
        return -1
    end

    if ( ( TempIntVal != 0 ) & ( TempIntVal != 1 ) )
        println( string( "  ERROR: Invalid flag. Line 14 - " , joinpath( path , file_name ) ) )
        return -1
    else
        instr.Simh = TempIntVal
    end

end;

#--- Funcao para fechar todas as conexoes abertas
function close_files( iofiles::Array{IOStream,1} )
    for iFile in iofiles
        Base.close( iFile )
    end
end;

#--- Funcao para leitura e conversao dos dados de um arquivos de entrada
function get_line_data!( iofile::IOStream , result::Vector{Float64} )

    #================================
    #---  Definicao de variaveis  ---
    ================================#

    local TempData      ::  String
    local TempArrData   ::  Array{SubString{String},1}

    #==================
    #---  Rotinas  ---
    ==================#

    #- Read data in line
    TempData = readline( iofile )
    
    #- Verifica validade dos dados
    if TempData == ""
        Base.println( "  ERROR: Expected some data at:")
        return -1
    end

    #- Converte dados para vetor
    try
       TempArrData = split( TempData , ',' ) 
    catch
        Base.println( "  Error: Invalid file format at:")
        return -1
    end

    #- Verifica consistencia dos dados antes de converter
    if length( TempArrData ) < ( length( result ) + 2 )
        Base.println( "  Error: Invalid number of scenarios at:")
        return -1
    end

    #- Converte dados para tipo desejado
    cont = 1
    try
        for i in 1:length( result )
            result[ i ] = parse( Float64 , TempArrData[ i + 2 ] )
            cont += 1
        end
    catch
        Base.println( "  Error: Invalid number format at:")
        Base.println( string( "    Scenario: " , cont ) )
        return  -1
    end

    return 0
end;

#--- read_line_data!: Funcao para leitura de uma etapa e bloco de todos os arquivos de entrada
function read_line_data!( iofiles::Array{IOStream,1} , result::Array{Float64,2} , stg::Int , blc::Int )

    #================================
    #---  Definicao de variaveis  ---
    ================================#

    local TempVec   ::  Vector{Float64};
    local iFile     ::  Int;
    
    TempVec = zeros( Float64 , size( result )[ 1 ] );

    #==================
    #---  Rotinas  ---
    ==================#

    for iFile in 1:length( iofiles );
        if get_line_data!( iofiles[ iFile ] , TempVec ) == -1;
            println( string( "    Stage: " , stg ) );
            println( string( "    Block: " , blc ) );
            close_files( iofiles );
            return -1;
        end
        result[ : , iFile ] = TempVec;
    end;

    return 0;
end;

#===========================
---  Funcoes Principais  ---
===========================#

#--- add_input_file!: Funcao para incluir arquivo como input
function add_input_file!( input_path::String , input_file_name::String , iofiles::Array{IOStream,1} )
    try;
        iofile = open( joinpath( input_path , string( input_file_name , ".csv" ) ) , "r" );
        push!( iofiles , iofile );
        readline( iofile );     # Skip header
    catch;
        println( string( "  ERROR: File not found" , joinpath( input_path , string( input_file_name , ".csv" ) ) ) );
        return -1;
    end;
    return 0;
end

#--- generate_graf_file: Funcao para gerar arquivo tipo Graf 
function generate_graf_file( instr::InstrConvGraf )

    #================================
    #---  Definicao de variaveis  ---
    ================================#

    local IoFiles       ::  Array{IOStream,1}
    local TempBlcStg    ::  Vector{Int}
    local TempData      ::  Array{Float64,2}

    #--- Cria vetores auxiliares
    IoFiles     = IOStream[];
    TempBlcStg  = get_block_stage( instr.IniStg , instr.IniYear , instr.NumStg , instr.NumBlc , instr.Simh , instr.TipoStg );
    TempData    = zeros( Float64 , instr.NumSer , length( instr.FilesInput ) );

    #==================
    #---  Rotinas  ---
    ==================#

    #--- Criando ponteiros para os arquivos de entrada
    for iFile in instr.FilesInput
        if add_input_file!( instr.PathInput , iFile , IoFiles ) == -1
            close_files( IoFiles )
            return -1
        end
    end

    #--- Configurando arquivo de saida
    IoOutFile = constr_output_file( instr )
    graph_save_init( IoOutFile )

    #--- Percorre dados
    for iStg in 1:instr.NumStg
        
        PSRIOGrafResultBase_setCurrentStage( IoOutFile.Ptr , iStg )         # Determina etapa

        TempNumBlc = TempBlcStg[ iStg ]                                     # Numero de blocos na etapa

        for iBlc in 1:TempNumBlc

            PSRIOGrafResultBase_setCurrentBlock( IoOutFile.Ptr , iBlc )     # Determina bloco

            #- Le dados de entrada
            if read_line_data!( IoFiles , TempData , iStg , iBlc ) == -1
                graph_close( IoOutFile , "save" )
                return -1
            end

            #- Aplica valores ao arquivo de saida
            for iSer in 1:instr.NumSer

                PSRIOGrafResultBase_setCurrentSerie( IoOutFile.Ptr , iSer ) # Determinada serie

                #- Percorre agentes
                for iAgente in 1:length( instr.Agentes )
                    PSRIOGrafResultBase_setData( IoOutFile.Ptr , iAgente - 1 , TempData[ iSer , iAgente ] )
                end

                PSRIOGrafResultBase_writeRegistry( IoOutFile.Ptr )          # Escreve dados no arquivo
            end
        end
    end

    close_files( IoFiles )
    graph_close( IoOutFile , "save" )

    return 0
end

#--- convgraf_main: Funcao principal
function convgraf_main( path_src::String )

    #================================
    #---  Definicao de variaveis  ---
    ================================#

    local FilesInDir    ::  Vector{String}
    local MyInstruc     ::  InstrConvGraf

    #==================
    #---  Rotinas  ---
    ==================#

    #--- Verifica se o path informado existe
    if check_path( path_src ) == -1
        println( string( "  ERROR: Output directory not found - " , path_src ) )
        return -1
    end

    #--- Obtendo arquivos no diretorio de execucao
    FilesInDir = readdir( path_src )

    #--- Processando arquivos
    println( "  Processing files..." )    

    for iFile in FilesInDir
        if length( iFile ) < 14
            # do nothing
        elseif ( ( iFile[1:8] == "convgraf" ) & ( iFile[12:15] == ".dat" ) )
            
            println( string( "    - " , iFile ) )

            #- Cria variavel de instrucoes
            MyInstruc = constr_instruc_var( path_src )

            #- Leitura do arquivo de instrucoes
            if read_instruc_file!( path_src , iFile , MyInstruc ) == -1
                println( string( "\n" , "\n" , "  WARNING: INSTRUCTIONS FROM THIS FILE WERE IGNORED!! -> " , iFile , "\n" ) )
            else
                generate_graf_file( MyInstruc )
            end
        end
    end

    #--- Finaliza rotina
    println( string( "\n" , "  DONE!" ) )

    return 0;
end

#--- run_main: Funcao para execução do modulo
function run_main( path_src::String )

    #- Loading PSRClasses DLL
    PSRClasses_init( joinpath( pathof( PSRClassesJulia ) , ".." , ".." , "deps" ) )

    #- Call main module
    convgraf_main( path_src )

end

